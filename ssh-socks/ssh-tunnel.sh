#!/bin/sh

if [ ! -z "$ID_RSA" ] && [ ! -z "$ID_RSA_PUB" ] && [ ! -z "$KNOWN_HOSTS" ] && [ ! -z "$SSH_TUNNEL_ENDPOINT" ] && [ ! -z $SSH_USER ]; then
    echo "Known_hosts and keys variables present. Setting up tunnel"
    mkdir -p /root/.ssh
    echo "$ID_RSA" > /root/.ssh/id_rsa
    echo "$ID_RSA_PUB" > /root/.ssh/id_rsa.pub
    echo "$KNOWN_HOSTS" > /root/.ssh/known_hosts
    chmod 700 /root/.ssh
    chmod 600 /root/.ssh/*
else 
    if [ -z $ID_RSA ]; then
        echo "No ID_RSA environment variable. Please fill it with your private unpassworded key"
    fi
    if [ -z $ID_RSA_PUB ]; then
        echo "No ID_RSA_PUB environment variable. Please fill it with your public key"        
    fi
    if [ -z $KNOWN_HOSTS ]; then
        echo "No KNOWN_HOSTS environment variable. Please fill it with public key of your server"
    fi
    if [ -z $SSH_USER ]; then
        echo "No SSH_USER environment variable. Please fill it with username on your server"
    fi
    if [ -z $SSH_TUNNEL_ENDPOINT ]; then
        echo "No SSH_TUNNEL_ENDPOINT environment variable. Please fill it with address of your server"
    fi

    # As docker have only "on-failure" restart condition, exit with normal exit code for prevent restart without reconfiguring
    exit 0
fi

echo "Starting up tunnel"
/usr/bin/ssh -vv -t -N -D *:1080 -l $SSH_USER $SSH_TUNNEL_ENDPOINT
echo "Tunnel stopped. Waiting 5 second"
sleep 5
echo "Exitting"
# As docker have only "on-failure" restart condition, exit with error code to force restart.
exit 1
