#!/bin/sh
if [ ! -z "$UPLINK_SOCKS_SERVER" ] && [ ! -z "$UPLINK_SOCKS_PORT" ] ; then
    echo "Uplink socks server and port are defined. Setting up proxy"
    cd /etc/privoxy
    sed 's/---UPLINK_SOCKS_SERVER---/'$UPLINK_SOCKS_SERVER'/; s/---UPLINK_SOCKS_PORT---/'$UPLINK_SOCKS_PORT'/' config.template > config
else 
    if [ -z $UPLINK_SOCKS_SERVER ]; then
        echo "No UPLINK_SOCKS_SERVER environment variable. Please fill it with socks uplink servername"
    fi
    if [ -z $UPLINK_SOCKS_PORT ]; then
        echo "No UPLINK_SOCKS_PORT environment variable. Please fill it with socks port"        
    fi
    # As docker have only "on-failure" restart condition, exit with normal exit code for prevent restart without reconfiguring
    exit 0
fi

echo "Starting up privoxy"
cd /etc/privoxy
privoxy --no-daemon
echo "Privoxy stopped. Waiting 5 second"
sleep 5
echo "Exitting"
# As docker have only "on-failure" restart condition, exit with error code to force restart.
exit 1
